const Hapi = require('@hapi/hapi')
const Boom = require('boom')

const init = async () => {
    const server = Hapi.server({
        port: 8080,
        host: 'localhost'
    })

    await server.register({
        plugin: require('hapi-mongodb'),
        options: {
            url: 'mongodb://localhost:27017/fruits',
            settings : {
                useUnifiedTopology: true
            },
            decorate: true
        }
    })

    server.route({
        method: 'GET',
        path: '/dbconnection',
        options: {
            cors: true
        },
        handler: (request, h) => {
            if(request.mongo.client.topology.isConnected()) {
                return h.response(true).code(200)
            } else {
                let error = new Error('Not connected')
                return Boom.boomify(error, { statusCode: 404 })
            }
        }
    })

    server.route({
        method: 'POST',
        path: '/dummydata',
        options: {
            cors: true
        },
        handler: (request, h) => {
            let dumyDate = {
                stringValue: generateString(Math.floor(Math.random() * 10)),
                numberValue: Math.floor(Math.random() * 100)
            }

            return h.response(dumyDate)
        }
    })

    server.route({
        method: 'GET',
        path: '/aggregatetime',
        options: {
            cors: true
        },
        handler: async (request, h) => {
            const db = request.mongo.db
            const start = Date.now()  
            const result = await db.collection('apples').aggregate([
                { $match: { "name": "Red Delicios"}}
            ])
            console.log(result)
            const end = Date.now()
            return `${end - start} ms`;
        }
    })

    server.route({
        method: 'GET',
        path: '/apples/{id}',
        async handler(request)  {
            const db = request.mongo.db
            console.log(request.mongo.client.topology.isConnected())
            const ObjectId = request.mongo.ObjectId
            try {
                const result = await db.collection('apples').findOne({ _id: new ObjectId(request.params.id)})
                return result
            } catch(err) {
                throw Boom.internal('Internal MongoDB error', err)
            }
        }
    })

    await server.start()
    console.log(`Server is running on ${server.info.uri}`)
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
})

function generateString(length) {
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    let result = '';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

init()