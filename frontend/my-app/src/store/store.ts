import {createStore} from 'redux';

export interface DummyData {
    stringValue: string,
    numberValue: number
}

export interface AppState {
    logged: boolean,
    username: string,
    password: string
    dummyData: DummyData
    aggregateTime: string
}

export const appState: AppState = {
    logged: false,
    username: "username@test.com",
    password: "123456",
    dummyData: {
        stringValue: "",
        numberValue: 0
    },
    aggregateTime: ''
}

function reducer(state = appState, action: Action) {
    switch(action.type) {
        case 'LOGIN':
            return {
                ...state,
                logged: action.payload
            }
        case 'DUMMY_DATA':
            return {
                ...state,
                dummyData: action.payload
            }
        case 'AGGREGATE_TIME':
            return {
                ...state,
                aggregateTime: action.payload
            }    
        default:
            return state    
    }
}

export function login(payload: boolean) {
    return {
        type: 'LOGIN',
        payload: payload
    }
}

export function changeDummyData(payload: DummyData) {
    return {
        type: 'DUMMY_DATA',
        payload: payload
    }
}

export function changeAggregateTime(payload: string) {
    return {
        type: 'AGGREGATE_TIME',
        payload: payload
    }
}

interface Action {
    type: string,
    payload: any
}

const store = createStore(reducer)

export default store