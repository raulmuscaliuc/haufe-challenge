import axios from "axios";
import { useDispatch } from "react-redux";
import { changeAggregateTime, changeDummyData, DummyData } from "../store/store";

export async function checkConnection() {
    const requestData = async () => {
        const response = await axios.get(
            `http://localhost:8080/dbconnection`
        )
        return response.data
    }

    const data = await requestData()
    return data

}

export async function useDummyData() {
    const dispatch = useDispatch()
    const requestData = async () => {
        const response = await axios.post(
            `http://localhost:8080/dummydata`
        )
        return response.data
    }

    const data: DummyData = await requestData()
        
    dispatch(changeDummyData(data))

    return data
}

export async function useAggregateTime() {
    const dispatch = useDispatch()
    const requestData = async () => {
        const response = await axios.get(
            `http://localhost:8080/aggregatetime`
        )
        return response.data
    }

    const data: string = await requestData()

    dispatch(changeAggregateTime(data))

    return data
}