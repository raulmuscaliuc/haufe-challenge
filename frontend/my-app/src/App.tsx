import './App.css';
import {BrowserRouter as Router, Link, Route, Routes} from 'react-router-dom';
import Login from './components/Login/Login';
import { useSelector } from 'react-redux';
import { AppState } from './store/store'
import { useAggregateTime, useDummyData } from './requests/mongoDbInfo';
import DummyDataView from './components/DummyData/DummyDataView';
import AggregateTimeView from './components/AggregateTime/AggregateTimeView';

function App() {
  useDummyData()
  useAggregateTime()

  const logged = useSelector((state: AppState) => state.logged)

  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/data">Generate data</Link>
          </li>
          <li>
            <Link to="/time-info">Aggregate time</Link>
          </li>
        </ul>
        <Routes>
          {!logged && <Route path='/' element={<Login/>}/>}
          {logged && <Route path='/' element={<h1>Home</h1>}/>}
          {logged && <Route path='/data' element={<DummyDataView/>}></Route>}
          {logged && <Route path='/time-info' element={<AggregateTimeView/>}></Route>}
        </Routes>
      </div>
    </Router>
  );
}

export default App;
