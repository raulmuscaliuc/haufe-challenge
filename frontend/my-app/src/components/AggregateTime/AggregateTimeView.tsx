import { useSelector } from "react-redux"
import { AppState } from "../../store/store"

function AggregateTimeView() {

    const aggregateTime = useSelector((state: AppState) => state.aggregateTime)

    return(
        <div>
            <p>Aggregate time is {aggregateTime}</p>
        </div>
    )
}

export default AggregateTimeView