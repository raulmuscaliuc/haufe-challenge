import { useSelector } from 'react-redux';
import { AppState } from '../../store/store'

function DummyDataView() {
    const stringValue = useSelector((state: AppState) => state.dummyData.stringValue)
    const numberValue = useSelector((state: AppState) => state.dummyData.numberValue)
    return (
        <div>
            <p>{stringValue}</p>
            <p>{numberValue}</p>
        </div>
    )
}

export default DummyDataView