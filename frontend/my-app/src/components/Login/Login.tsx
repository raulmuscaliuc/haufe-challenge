import React, {useState} from 'react'
import { useDispatch } from 'react-redux'
import { checkConnection } from '../../requests/mongoDbInfo'
import store, { login } from '../../store/store'

function Login() {
  const state = store.getState()
  const defaultUsername = state.username
  const defaultePassword = state.password
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()

  const handleSubimt = (event: any) => {
    const dbConnection = checkConnection()
    event.preventDefault()
    if(username === defaultUsername && password === defaultePassword && dbConnection) {
      dispatch(login(true))
    }
  }

  return(
    <div className="form">
     <form onSubmit={handleSubimt}>
       <div className="input-container">
         <label>Username </label>
         <input 
          type="text" 
          name="uname" 
          required
          onChange={e => setUsername(e.target.value)}
         />
       </div>
       <div className="input-container">
         <label>Password </label>
         <input 
          type="password"  
          name="pass" 
          required
          onChange={e => setPassword(e.target.value)} 
         />
       </div>
       <div className="button-container">
         <input type="submit" />
       </div>
     </form>
   </div>
  )
} 

export default Login;
